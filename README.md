# React 16 DB Monster using React Minimal

### What is React Minimal?
React Minimal is a simple boilerplate code to get started with React JS with ES6. It uses Babel to transpile ES6, WebPack for bundling.

### What is DB Monster?
So, a popular way to compare the performance of frameworks a few years ago was DBMonster, which was first introduced at React.js conf 2015: https://www.youtube.com/watch?v=z5e7kWSHWTg You can find various implementations in your favorite frameworks online. For example, you can find a bunch of old comparisons here: https://mathieuancelin.github.io/js-repaint-perfs/

I've basically just dropped in the js implementation of react-dbmon found here: https://github.com/localvoid/react-dbmon
but with react 16 instead of 15

# Installation?

  - Clone the repository
  - Install the required modules using "npm install"
  - Run "npm run dev" for webpack with watcher
  - Run "npm run build" without watcher
  - Open the generated index.html file inside the build folder

Detailed article about using this [React JS module](https://www.thamizhchelvan.com/javascript/react-minimal-boilerplate/) is given here.

## License

MIT

React Minimal is Copyright (c) 2017 [Thamizhchelvan Govindan](http://www.thamizhchelvan.com/).

I think I can just say this repo's under the same as above...